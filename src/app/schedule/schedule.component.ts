import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ScheduleItemModel } from './schedule-item.model';
import { ScheduleService } from 'src/app/schedule/schedule.service';
import { TemplateModeEnum } from 'src/app/shared/enums/template-mode.enum';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
    templateUrl: 'schedule.component.html',
    selector: 'app-schedule'
})
export class ScheduleComponent implements OnInit, OnDestroy {
    @ViewChild('f') schForm: NgForm;
    schedule: ScheduleItemModel[];
    mode: TemplateModeEnum;
    modeEnum = TemplateModeEnum;
    subscriptions: Subscription[];


    constructor(
        private scheduleService: ScheduleService
    ) {
        this.mode = this.modeEnum.Display;
        this.subscriptions = new Array<Subscription>();
    }
    
    ngOnInit() {
        this.subscriptions.push(
        this.scheduleService.calculateSchedule(120)
        .subscribe( (response: ScheduleItemModel[]) => {
            this.schedule = response;
        })
    );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => {
            subscription.unsubscribe();
        });
    }

    onEditMode() {
        this.mode = this.modeEnum.Edition;
        // this.schForm.setValue({
        //     startTime: '14:00',
        //     endTime: '16:30'
        // });
    }

    onCancel() {
        this.mode = this.modeEnum.Display;
    }

    onSave() {
        this.mode = this.modeEnum.Display;
    }
}
