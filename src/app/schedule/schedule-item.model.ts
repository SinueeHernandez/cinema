export interface IScheduleItemModel {
    startTime: string;
    endTime: string;
}

export class ScheduleItemModel {
    startTime: string;
    endTime: string;

    constructor(obj?: IScheduleItemModel) {
        this.startTime = obj && obj.startTime || '12:00';
        this.endTime = obj && obj.endTime || '14:00';
    }
}