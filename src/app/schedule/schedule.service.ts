import { Injectable, OnDestroy } from '@angular/core';
import { ScheduleItemModel } from './schedule-item.model';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ScheduleService implements OnDestroy {

    schedule: ScheduleItemModel[];
    url = 'https://cinema-77144.firebaseio.com/schedule.json';
    subscriptions: Subscription[];

    constructor (
        private httpClient: HttpClient
    ) {
        this.subscriptions = new Array<Subscription>();
     }

    ngOnDestroy(){
        this.subscriptions.forEach(subscription => {
            subscription.unsubscribe();
        });
    }

    calculateSchedule(movieDuration: number) {
        this.schedule = new Array<ScheduleItemModel>();
        const subs = this.httpClient.get(this.url);
        
        return subs;
    }
}