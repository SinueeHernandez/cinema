import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ScreenModeEnum } from './screen-mode.enum';
import { ScreenModel } from './screen.model';

@Component({
    selector: 'app-screen',
    templateUrl: 'screen.component.html'
})
export class ScreenComponent implements OnInit {
    @Input() screen: ScreenModel;
    @Input() index: number;
    mode: ScreenModeEnum;
    screenModeEnum = ScreenModeEnum;

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.route.params
        .subscribe((params: Params) => {
            this.index = +params['index'];
            if (params['index'] === undefined) {
                this.mode = ScreenModeEnum.List;
            } else {
                this.mode = ScreenModeEnum.Single;
            }

        });
    }

    ngOnInit() {
    }

    goHome() {
        this.router.navigate(['/']);
    }
}
