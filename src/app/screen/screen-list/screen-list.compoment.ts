import { Component, Input } from '@angular/core';
import { ScreenModel } from '../screen.model';

@Component({
    selector: 'app-screen-list',
    template: `
    <div  *ngFor="let screen of screens; let i = index" >
      <app-screen [screen]="screen" [index]="i" ></app-screen>
    </div>
    `
})
export class ScreenListComponent {
  @Input() screens: ScreenModel[];

    constructor() {}

}
