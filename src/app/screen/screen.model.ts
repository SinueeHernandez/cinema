import { ScheduleItemModel } from '../schedule/schedule-item.model';

export class ScreenModel {
    schedule: ScheduleItemModel[];
    capacity: number;

    constructor() {
        this.schedule = [
            new ScheduleItemModel(),
            new ScheduleItemModel(),
            new ScheduleItemModel(),
            new ScheduleItemModel(),
            new ScheduleItemModel()
        ];
        this.capacity = 113;
    }
}
