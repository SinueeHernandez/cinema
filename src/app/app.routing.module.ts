import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScreenComponent } from './screen/screen.component';
import { HomeComponent } from './core/home/home.component';
import { MovieListComponent } from './movie/movie-list/movie-list.component';
import { MovieComponent } from './movie/movie.component';

export const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'movies', component: MovieListComponent},
    { path: 'movies/:index', component: MovieComponent},
    { path: ':index/screen', component: ScreenComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
