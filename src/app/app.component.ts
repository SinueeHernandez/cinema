import { Component, OnInit } from '@angular/core';
import { initializeApp } from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  ngOnInit() {
    const config = {
      apiKey: "AIzaSyA3hviUXB1LOocQexMqrF8aRilYF9HVqjk",
      authDomain: "cinema-77144.firebaseapp.com",
      databaseURL: "https://cinema-77144.firebaseio.com",
      projectId: "cinema-77144",
      storageBucket: "cinema-77144.appspot.com",
      messagingSenderId: "988356765820"
    };
    initializeApp(config);
  }

}
