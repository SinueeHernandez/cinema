import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { Router } from '@angular/router';
import { appRoutes } from './app.routing.module';
import { HomeComponent } from './core/home/home.component';
import { MovieListComponent } from './movie/movie-list/movie-list.component';
import { MovieComponent } from './movie/movie.component';
import { ScreenComponent } from './screen/screen.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF, Location } from '@angular/common';
import { MovieService } from './movie/movie.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('Router: App', () => {

    let location: Location;
    let router: Router;
    let fixture;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                HeaderComponent,
                HomeComponent,
                MovieListComponent,
                MovieComponent,
                ScreenComponent,
                ScheduleComponent,
                AppComponent
            ],
            imports: [
                FormsModule,
                RouterTestingModule.withRoutes(appRoutes)
            ],
            providers: [
                { provide: APP_BASE_HREF, useValue: '/' },
                MovieService,
                HttpClient,
                HttpHandler
            ]
        }).compileComponents();

        router = TestBed.get(Router);
        location = TestBed.get(Location);

        fixture = TestBed.createComponent(AppComponent);
        router.initialNavigation();
    }));

    it('fakeAsync works', fakeAsync(() => {
        const promise = new Promise((resolve) => {
            setTimeout(resolve, 10);
        });
        let done = false;
        promise.then(() => done = true);
        tick(50);
        expect(done).toBeTruthy();
    }));

    it('navigate to "" redirects you to /', fakeAsync(() => {
        router.navigate(['']);
        tick(50);
        expect(location.path()).toBe('/');
    }));

    it('navigate to "movies" takes you to /movies', fakeAsync(() => {
        router.navigate(['/movies']);
        tick(50);
        expect(location.path()).toBe('/movies');
    }));
});
