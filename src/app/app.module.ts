import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MovieService } from './movie/movie.service';
import { ScreenComponent } from './screen/screen.component';
import { ScreenListComponent } from './screen/screen-list/screen-list.compoment';
import { AppRoutingModule } from './app.routing.module';
import { ScheduleComponent } from './schedule/schedule.component';
import { CoreModule } from './core/core.module';
import { MovieModule } from './movie/movie.module';

@NgModule({
  declarations: [
    AppComponent,
    ScreenComponent,
    ScreenListComponent,
    ScheduleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    MovieModule
  ],
  providers: [
    MovieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
