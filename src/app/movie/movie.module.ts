import { NgModule } from '@angular/core';
import { MovieComponent } from './movie.component';
import { MovieService } from './movie.service';
import { MovieListComponent } from './movie-list/movie-list.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
    MovieComponent,
    MovieListComponent
    ],
    exports: [
        MovieComponent,
        MovieListComponent
    ],
    imports: [
        CommonModule,
        RouterModule
    ],
    providers: [
        MovieService
    ]
})
export class MovieModule {}
