import { ScreenModel } from '../screen/screen.model';

export class MovieModel {
    name: string;
    year: number;
    director: string;
    posterUrl: string;
    description: string;
    duration: number;
    screens: ScreenModel[];

    constructor() {
        this.name = 'AAA';
        this.director = 'Some Director';
        this.year = 1910;
        this.posterUrl = 'https://officedepot.scene7.com/is/image/officedepot/858430_p?$OD%2DLarge$&wid=450&hei=450';
        this.description = '';
        this.duration = 120;
        this.screens = [new ScreenModel(), new ScreenModel(), new ScreenModel()];
    }
}
