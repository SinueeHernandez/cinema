import { Component, Input, OnInit } from '@angular/core';
import { MovieModel } from './movie.model';
import { ScreenModeEnum } from '../screen/screen-mode.enum';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MovieService } from './movie.service';

@Component({
    templateUrl: 'movie.component.html',
    selector: 'app-movie'
})
export class MovieComponent implements OnInit {
    @Input() movie: MovieModel;
    @Input() index: number;
    mode: ScreenModeEnum;
    screenModeEnum = ScreenModeEnum;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private movieService: MovieService
    ) {
        this.route.params
            .subscribe((params: Params) => {
                this.index = +params['index'];
                if (params['index'] === undefined) {
                    this.mode = ScreenModeEnum.List;
                } else {
                    this.mode = ScreenModeEnum.Single;
                }
            });
    }

    ngOnInit() {
        if (this.mode === ScreenModeEnum.Single) {
            this.movieService.getMovieByIndex(this.index)
            .subscribe((movie: MovieModel) => {
                // this.movie = movie;
            });
        }
    }

}
