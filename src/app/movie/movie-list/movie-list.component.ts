import { Component, OnInit } from '@angular/core';
import { MovieModel } from '../movie.model';
import { MovieService } from '../movie.service';

@Component({
    templateUrl: 'movie-list.component.html',
    selector: 'app-movie-list'
})
export class MovieListComponent implements OnInit {
    movies: MovieModel[];

    constructor(
        private movieService: MovieService
    ) {}

    ngOnInit() {
        this.movieService.getMovies()
        .subscribe((movies: MovieModel[]) => {
            this.movies = movies;
        });
    }

    saveMovies() {
        this.movieService.updateMovies()
        .subscribe(() => {
            console.log('Movies saved');
        });
    }
}
