import { Injectable } from '@angular/core';
import { MovieModel } from './movie.model';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable()
export class MovieService {
    public movies: MovieModel[];
    private url = 'https://cinema-77144.firebaseio.com/movies';

    constructor(
        private http: HttpClient
    ) {
        this.movies = [];
    }

    public getMovies() {
        return this.http.get(`${this.url}.json`)
            .pipe(
                tap((movies: MovieModel[]) => {
                    this.movies = movies;
                })
            );
    }

    public getMovieByIndex(index: number) {
        return this.http.get(`${this.url}/${index}.json`);
    }

    public updateMovies() {
        return this.http.put(`${this.url}.json`, this.movies);
    }
}
