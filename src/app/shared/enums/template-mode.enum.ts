export enum TemplateModeEnum {
    Edition = 'edit',
    Display = 'display'
}
